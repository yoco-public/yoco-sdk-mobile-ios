//
//  NSData+Utils.h
//  YocoSDK
//
//  Created by Nkokhelo Mhlongo on 7/24/18.
//  Copyright © 2018 Yoco. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (Utils)

+(BOOL)isEmpty:(NSData*)data;
+(BOOL)isNotEmpty:(NSData*)data;

@end
