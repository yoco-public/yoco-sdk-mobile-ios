// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.10 effective-4.2 (swiftlang-5.10.0.13 clang-1500.3.9.4)
// swift-module-flags: -target arm64-apple-ios12.0 -enable-objc-interop -enable-library-evolution -swift-version 4.2 -enforce-exclusivity=checked -O -enable-bare-slash-regex -module-name YCReSwift
import Foundation
import Swift
import _Concurrency
import _StringProcessing
import _SwiftConcurrencyShims
public protocol AnyStoreSubscriber : AnyObject {
  func _newState(state: Any)
}
public protocol StoreSubscriber : YCReSwift.AnyStoreSubscriber {
  associatedtype StoreSubscriberStateType
  func newState(state: Self.StoreSubscriberStateType)
}
extension YCReSwift.StoreSubscriber {
  public func _newState(state: Any)
}
open class Store<State> : YCReSwift.StoreType where State : YCReSwift.StateType {
  public var state: State! {
    get
  }
  public var dispatchFunction: YCReSwift.DispatchFunction! {
    get
    set
  }
  public var middleware: [YCReSwift.Middleware<State>] {
    get
    set
  }
  required public init(reducer: @escaping YCReSwift.Reducer<State>, state: State?, middleware: [YCReSwift.Middleware<State>] = [], automaticallySkipsRepeats: Swift.Bool = true)
  open func subscribe<S>(_ subscriber: S) where State == S.StoreSubscriberStateType, S : YCReSwift.StoreSubscriber
  open func subscribe<SelectedState, S>(_ subscriber: S, transform: ((YCReSwift.Subscription<State>) -> YCReSwift.Subscription<SelectedState>)?) where SelectedState == S.StoreSubscriberStateType, S : YCReSwift.StoreSubscriber
  open func unsubscribe(_ subscriber: any YCReSwift.AnyStoreSubscriber)
  open func _defaultDispatch(action: any YCReSwift.Action)
  open func dispatch(_ action: any YCReSwift.Action)
  @available(*, deprecated, message: "Deprecated in favor of https://github.com/ReSwift/ReSwift-Thunk")
  open func dispatch(_ actionCreatorProvider: @escaping YCReSwift.Store<State>.ActionCreator)
  @available(*, deprecated, message: "Deprecated in favor of https://github.com/ReSwift/ReSwift-Thunk")
  open func dispatch(_ asyncActionCreatorProvider: @escaping YCReSwift.Store<State>.AsyncActionCreator)
  @available(*, deprecated, message: "Deprecated in favor of https://github.com/ReSwift/ReSwift-Thunk")
  open func dispatch(_ actionCreatorProvider: @escaping YCReSwift.Store<State>.AsyncActionCreator, callback: YCReSwift.Store<State>.DispatchCallback?)
  public typealias DispatchCallback = (State) -> Swift.Void
  @available(*, deprecated, message: "Deprecated in favor of https://github.com/ReSwift/ReSwift-Thunk")
  public typealias ActionCreator = (_ state: State, _ store: YCReSwift.Store<State>) -> (any YCReSwift.Action)?
  @available(*, deprecated, message: "Deprecated in favor of https://github.com/ReSwift/ReSwift-Thunk")
  public typealias AsyncActionCreator = (_ state: State, _ store: YCReSwift.Store<State>, _ actionCreatorCallback: @escaping (((_ state: State, _ store: YCReSwift.Store<State>) -> (any YCReSwift.Action)?) -> Swift.Void)) -> Swift.Void
  @objc deinit
}
extension YCReSwift.Store {
  open func subscribe<SelectedState, S>(_ subscriber: S, transform: ((YCReSwift.Subscription<State>) -> YCReSwift.Subscription<SelectedState>)?) where SelectedState : Swift.Equatable, SelectedState == S.StoreSubscriberStateType, S : YCReSwift.StoreSubscriber
}
extension YCReSwift.Store where State : Swift.Equatable {
  open func subscribe<S>(_ subscriber: S) where State == S.StoreSubscriberStateType, S : YCReSwift.StoreSubscriber
}
public protocol Coding {
  init?(dictionary: [Swift.String : Swift.AnyObject])
  var dictionaryRepresentation: [Swift.String : Swift.AnyObject] { get }
}
public protocol Action {
}
public struct ReSwiftInit : YCReSwift.Action {
}
public typealias DispatchFunction = (any YCReSwift.Action) -> Swift.Void
public typealias Middleware<State> = (@escaping YCReSwift.DispatchFunction, @escaping () -> State?) -> (@escaping YCReSwift.DispatchFunction) -> YCReSwift.DispatchFunction
public protocol StoreType : YCReSwift.DispatchingStoreType {
  associatedtype State : YCReSwift.StateType
  var state: Self.State! { get }
  var dispatchFunction: YCReSwift.DispatchFunction! { get }
  func subscribe<S>(_ subscriber: S) where S : YCReSwift.StoreSubscriber, Self.State == S.StoreSubscriberStateType
  func subscribe<SelectedState, S>(_ subscriber: S, transform: ((YCReSwift.Subscription<Self.State>) -> YCReSwift.Subscription<SelectedState>)?) where SelectedState == S.StoreSubscriberStateType, S : YCReSwift.StoreSubscriber
  func unsubscribe(_ subscriber: any YCReSwift.AnyStoreSubscriber)
  func dispatch(_ actionCreator: Self.ActionCreator)
  func dispatch(_ asyncActionCreator: Self.AsyncActionCreator)
  func dispatch(_ asyncActionCreator: Self.AsyncActionCreator, callback: Self.DispatchCallback?)
  associatedtype DispatchCallback = (Self.State) -> Swift.Void
  associatedtype ActionCreator = (_ state: Self.State, _ store: any YCReSwift.StoreType) -> (any YCReSwift.Action)?
  associatedtype AsyncActionCreator = (_ state: Self.State, _ store: any YCReSwift.StoreType, _ actionCreatorCallback: (Self.ActionCreator) -> Swift.Void) -> Swift.Void
}
@_hasMissingDesignatedInitializers public class Subscription<State> {
  public init(sink: @escaping (@escaping (State?, State) -> Swift.Void) -> Swift.Void)
  public func select<Substate>(_ selector: @escaping (State) -> Substate) -> YCReSwift.Subscription<Substate>
  public func select<Substate>(_ keyPath: Swift.KeyPath<State, Substate>) -> YCReSwift.Subscription<Substate>
  public func skipRepeats(_ isRepeat: @escaping (_ oldState: State, _ newState: State) -> Swift.Bool) -> YCReSwift.Subscription<State>
  public var observer: ((State?, State) -> Swift.Void)?
  @objc deinit
}
extension YCReSwift.Subscription where State : Swift.Equatable {
  public func skipRepeats() -> YCReSwift.Subscription<State>
}
extension YCReSwift.Subscription {
  public func skip(when: @escaping (_ oldState: State, _ newState: State) -> Swift.Bool) -> YCReSwift.Subscription<State>
  public func only(when: @escaping (_ oldState: State, _ newState: State) -> Swift.Bool) -> YCReSwift.Subscription<State>
}
public typealias Reducer<ReducerStateType> = (_ action: any YCReSwift.Action, _ state: ReducerStateType?) -> ReducerStateType
public protocol DispatchingStoreType {
  func dispatch(_ action: any YCReSwift.Action)
}
public protocol StateType {
}
