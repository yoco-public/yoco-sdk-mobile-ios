// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.10 (swiftlang-5.10.0.13 clang-1500.3.9.4)
// swift-module-flags: -target arm64-apple-ios9.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -enable-bare-slash-regex -module-name YCLottie
import CoreGraphics
import CoreText
import Foundation
import QuartzCore
import Swift
import UIKit
import _Concurrency
import _StringProcessing
import _SwiftConcurrencyShims
extension YCLottie.AnimationView {
  @_Concurrency.MainActor(unsafe) convenience public init(name: Swift.String, bundle: Foundation.Bundle = Bundle.main, imageProvider: (any YCLottie.AnimationImageProvider)? = nil, animationCache: (any YCLottie.AnimationCacheProvider)? = LRUAnimationCache.sharedCache)
  @_Concurrency.MainActor(unsafe) convenience public init(filePath: Swift.String, imageProvider: (any YCLottie.AnimationImageProvider)? = nil, animationCache: (any YCLottie.AnimationCacheProvider)? = LRUAnimationCache.sharedCache)
  @_Concurrency.MainActor(unsafe) convenience public init(url: Foundation.URL, imageProvider: (any YCLottie.AnimationImageProvider)? = nil, closure: @escaping YCLottie.AnimationView.DownloadClosure, animationCache: (any YCLottie.AnimationCacheProvider)? = LRUAnimationCache.sharedCache)
  public typealias DownloadClosure = ((any Swift.Error)?) -> Swift.Void
}
public enum LottieBackgroundBehavior {
  case stop
  case pause
  case pauseAndRestore
  case forceFinish
  public static func == (a: YCLottie.LottieBackgroundBehavior, b: YCLottie.LottieBackgroundBehavior) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public enum LottieLoopMode {
  case playOnce
  case loop
  case autoReverse
  case `repeat`(Swift.Float)
  case repeatBackwards(Swift.Float)
}
extension YCLottie.LottieLoopMode : Swift.Equatable {
  public static func == (lhs: YCLottie.LottieLoopMode, rhs: YCLottie.LottieLoopMode) -> Swift.Bool
}
@objc @_inheritsConvenienceInitializers @IBDesignable @_Concurrency.MainActor(unsafe) final public class AnimationView : YCLottie.LottieView {
  @_Concurrency.MainActor(unsafe) final public var animation: YCLottie.Animation? {
    get
    set
  }
  @_Concurrency.MainActor(unsafe) final public var backgroundBehavior: YCLottie.LottieBackgroundBehavior
  @_Concurrency.MainActor(unsafe) final public var imageProvider: any YCLottie.AnimationImageProvider {
    get
    set
  }
  @_Concurrency.MainActor(unsafe) final public var textProvider: any YCLottie.AnimationTextProvider {
    get
    set
  }
  @_Concurrency.MainActor(unsafe) final public var fontProvider: any YCLottie.AnimationFontProvider {
    get
    set
  }
  @_Concurrency.MainActor(unsafe) final public var isAnimationPlaying: Swift.Bool {
    get
  }
  @_Concurrency.MainActor(unsafe) final public var isAnimationQueued: Swift.Bool {
    get
  }
  @_Concurrency.MainActor(unsafe) final public var loopMode: YCLottie.LottieLoopMode {
    get
    set
  }
  @_Concurrency.MainActor(unsafe) final public var shouldRasterizeWhenIdle: Swift.Bool {
    get
    set
  }
  @_Concurrency.MainActor(unsafe) final public var currentProgress: YCLottie.AnimationProgressTime {
    get
    set
  }
  @_Concurrency.MainActor(unsafe) final public var currentTime: Foundation.TimeInterval {
    get
    set
  }
  @_Concurrency.MainActor(unsafe) final public var currentFrame: YCLottie.AnimationFrameTime {
    get
    set
  }
  @_Concurrency.MainActor(unsafe) final public var realtimeAnimationFrame: YCLottie.AnimationFrameTime {
    get
  }
  @_Concurrency.MainActor(unsafe) final public var realtimeAnimationProgress: YCLottie.AnimationProgressTime {
    get
  }
  @_Concurrency.MainActor(unsafe) final public var animationSpeed: CoreFoundation.CGFloat {
    get
    set
  }
  @_Concurrency.MainActor(unsafe) final public var respectAnimationFrameRate: Swift.Bool {
    get
    set
  }
  @_Concurrency.MainActor(unsafe) final public var viewportFrame: CoreFoundation.CGRect? {
    get
    set
  }
  @_Concurrency.MainActor(unsafe) final public func play(completion: YCLottie.LottieCompletionBlock? = nil)
  @_Concurrency.MainActor(unsafe) final public func play(fromProgress: YCLottie.AnimationProgressTime? = nil, toProgress: YCLottie.AnimationProgressTime, loopMode: YCLottie.LottieLoopMode? = nil, completion: YCLottie.LottieCompletionBlock? = nil)
  @_Concurrency.MainActor(unsafe) final public func play(fromFrame: YCLottie.AnimationFrameTime? = nil, toFrame: YCLottie.AnimationFrameTime, loopMode: YCLottie.LottieLoopMode? = nil, completion: YCLottie.LottieCompletionBlock? = nil)
  @_Concurrency.MainActor(unsafe) final public func play(fromMarker: Swift.String? = nil, toMarker: Swift.String, loopMode: YCLottie.LottieLoopMode? = nil, completion: YCLottie.LottieCompletionBlock? = nil)
  @_Concurrency.MainActor(unsafe) final public func stop()
  @_Concurrency.MainActor(unsafe) final public func pause()
  @_Concurrency.MainActor(unsafe) final public func reloadImages()
  @_Concurrency.MainActor(unsafe) final public func forceDisplayUpdate()
  @_Concurrency.MainActor(unsafe) final public func setValueProvider(_ valueProvider: any YCLottie.AnyValueProvider, keypath: YCLottie.AnimationKeypath)
  @_Concurrency.MainActor(unsafe) final public func getValue(for keypath: YCLottie.AnimationKeypath, atFrame: YCLottie.AnimationFrameTime?) -> Any?
  @_Concurrency.MainActor(unsafe) final public func logHierarchyKeypaths()
  @_Concurrency.MainActor(unsafe) final public func addSubview(_ subview: YCLottie.AnimationSubview, forLayerAt keypath: YCLottie.AnimationKeypath)
  @_Concurrency.MainActor(unsafe) final public func convert(_ rect: CoreFoundation.CGRect, toLayerAt keypath: YCLottie.AnimationKeypath?) -> CoreFoundation.CGRect?
  @_Concurrency.MainActor(unsafe) final public func convert(_ point: CoreFoundation.CGPoint, toLayerAt keypath: YCLottie.AnimationKeypath?) -> CoreFoundation.CGPoint?
  @_Concurrency.MainActor(unsafe) final public func setNodeIsEnabled(isEnabled: Swift.Bool, keypath: YCLottie.AnimationKeypath)
  @_Concurrency.MainActor(unsafe) final public func progressTime(forMarker named: Swift.String) -> YCLottie.AnimationProgressTime?
  @_Concurrency.MainActor(unsafe) final public func frameTime(forMarker named: Swift.String) -> YCLottie.AnimationFrameTime?
  @_Concurrency.MainActor(unsafe) public init(animation: YCLottie.Animation?, imageProvider: (any YCLottie.AnimationImageProvider)? = nil, textProvider: any YCLottie.AnimationTextProvider = DefaultTextProvider(), fontProvider: any YCLottie.AnimationFontProvider = DefaultFontProvider())
  @_Concurrency.MainActor(unsafe) @objc dynamic public init()
  @_Concurrency.MainActor(unsafe) @objc override dynamic public init(frame: CoreFoundation.CGRect)
  @_Concurrency.MainActor(unsafe) @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @_Concurrency.MainActor(unsafe) @objc override final public var intrinsicContentSize: CoreFoundation.CGSize {
    @objc get
  }
  @objc deinit
}
extension YCLottie.Animation {
  public static func named(_ name: Swift.String, bundle: Foundation.Bundle = Bundle.main, subdirectory: Swift.String? = nil, animationCache: (any YCLottie.AnimationCacheProvider)? = nil) -> YCLottie.Animation?
  public static func filepath(_ filepath: Swift.String, animationCache: (any YCLottie.AnimationCacheProvider)? = nil) -> YCLottie.Animation?
  public typealias DownloadClosure = (YCLottie.Animation?) -> Swift.Void
  public static func loadedFrom(url: Foundation.URL, closure: @escaping YCLottie.Animation.DownloadClosure, animationCache: (any YCLottie.AnimationCacheProvider)?)
  final public func progressTime(forMarker named: Swift.String) -> YCLottie.AnimationProgressTime?
  final public func frameTime(forMarker named: Swift.String) -> YCLottie.AnimationFrameTime?
  final public func progressTime(forFrame frameTime: YCLottie.AnimationFrameTime) -> YCLottie.AnimationProgressTime
  final public func frameTime(forProgress progressTime: YCLottie.AnimationProgressTime) -> YCLottie.AnimationFrameTime
  final public func time(forFrame frameTime: YCLottie.AnimationFrameTime) -> Foundation.TimeInterval
  final public func frameTime(forTime time: Foundation.TimeInterval) -> YCLottie.AnimationFrameTime
  final public var duration: Foundation.TimeInterval {
    get
  }
  final public var bounds: CoreFoundation.CGRect {
    get
  }
  final public var size: CoreFoundation.CGSize {
    get
  }
}
public protocol AnimationImageProvider {
  func imageForAsset(asset: YCLottie.ImageAsset) -> CoreGraphics.CGImage?
}
public class FilepathImageProvider : YCLottie.AnimationImageProvider {
  public init(filepath: Swift.String)
  public init(filepath: Foundation.URL)
  public func imageForAsset(asset: YCLottie.ImageAsset) -> CoreGraphics.CGImage?
  @objc deinit
}
@objc @_inheritsConvenienceInitializers @_Concurrency.MainActor(unsafe) final public class AnimatedSwitch : YCLottie.AnimatedControl {
  public enum CancelBehavior {
    case reverse
    case none
    public static func == (a: YCLottie.AnimatedSwitch.CancelBehavior, b: YCLottie.AnimatedSwitch.CancelBehavior) -> Swift.Bool
    public func hash(into hasher: inout Swift.Hasher)
    public var hashValue: Swift.Int {
      get
    }
  }
  @_Concurrency.MainActor(unsafe) final public var isOn: Swift.Bool {
    get
    set
  }
  @_Concurrency.MainActor(unsafe) final public var cancelBehavior: YCLottie.AnimatedSwitch.CancelBehavior
  @_Concurrency.MainActor(unsafe) final public func setIsOn(_ isOn: Swift.Bool, animated: Swift.Bool, shouldFireHaptics: Swift.Bool = true)
  @_Concurrency.MainActor(unsafe) final public func setProgressForState(fromProgress: YCLottie.AnimationProgressTime, toProgress: YCLottie.AnimationProgressTime, forOnState: Swift.Bool)
  @_Concurrency.MainActor(unsafe) override public init(animation: YCLottie.Animation)
  @_Concurrency.MainActor(unsafe) @objc override dynamic public init()
  @_Concurrency.MainActor(unsafe) @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @_Concurrency.MainActor(unsafe) @objc override final public func endTracking(_ touch: UIKit.UITouch?, with event: UIKit.UIEvent?)
  @_Concurrency.MainActor(unsafe) override final public func animationDidSet()
  @objc deinit
}
public class BundleImageProvider : YCLottie.AnimationImageProvider {
  public init(bundle: Foundation.Bundle, searchPath: Swift.String?)
  public func imageForAsset(asset: YCLottie.ImageAsset) -> CoreGraphics.CGImage?
  @objc deinit
}
extension UIKit.UIColor {
  public var lottieColorValue: YCLottie.Color {
    get
  }
}
@objc @_inheritsConvenienceInitializers @_Concurrency.MainActor(unsafe) final public class AnimatedButton : YCLottie.AnimatedControl {
  @_Concurrency.MainActor(unsafe) final public func setPlayRange(fromProgress: YCLottie.AnimationProgressTime, toProgress: YCLottie.AnimationProgressTime, event: UIKit.UIControl.Event)
  @_Concurrency.MainActor(unsafe) final public func setPlayRange(fromMarker fromName: Swift.String, toMarker toName: Swift.String, event: UIKit.UIControl.Event)
  @_Concurrency.MainActor(unsafe) override public init(animation: YCLottie.Animation)
  @_Concurrency.MainActor(unsafe) @objc override dynamic public init()
  @_Concurrency.MainActor(unsafe) @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @_Concurrency.MainActor(unsafe) @objc override final public func beginTracking(_ touch: UIKit.UITouch, with event: UIKit.UIEvent?) -> Swift.Bool
  @_Concurrency.MainActor(unsafe) @objc override final public func endTracking(_ touch: UIKit.UITouch?, with event: UIKit.UIEvent?)
  @objc deinit
}
@objc @_inheritsConvenienceInitializers @_Concurrency.MainActor(unsafe) open class LottieView : UIKit.UIView {
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func didMoveToWindow()
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var contentMode: UIKit.UIView.ContentMode {
    @objc get
    @objc set
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func layoutSubviews()
  @_Concurrency.MainActor(unsafe) @objc override dynamic public init(frame: CoreFoundation.CGRect)
  @_Concurrency.MainActor(unsafe) @objc required dynamic public init?(coder: Foundation.NSCoder)
  @objc deinit
}
@objc @_inheritsConvenienceInitializers @_Concurrency.MainActor(unsafe) final public class AnimationSubview : UIKit.UIView {
  @_Concurrency.MainActor(unsafe) @objc override dynamic public init(frame: CoreFoundation.CGRect)
  @_Concurrency.MainActor(unsafe) @objc required dynamic public init?(coder: Foundation.NSCoder)
  @objc deinit
}
@objc @_Concurrency.MainActor(unsafe) open class AnimatedControl : UIKit.UIControl {
  @_Concurrency.MainActor(unsafe) final public let animationView: YCLottie.AnimationView
  @_Concurrency.MainActor(unsafe) public var animation: YCLottie.Animation? {
    get
    set
  }
  @_Concurrency.MainActor(unsafe) public var animationSpeed: CoreFoundation.CGFloat {
    get
    set
  }
  @_Concurrency.MainActor(unsafe) public func setLayer(named: Swift.String, forState: UIKit.UIControl.State)
  @_Concurrency.MainActor(unsafe) public func setValueProvider(_ valueProvider: any YCLottie.AnyValueProvider, keypath: YCLottie.AnimationKeypath)
  @_Concurrency.MainActor(unsafe) public init(animation: YCLottie.Animation)
  @_Concurrency.MainActor(unsafe) @objc dynamic public init()
  @_Concurrency.MainActor(unsafe) @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var isEnabled: Swift.Bool {
    @objc get
    @objc set
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var isSelected: Swift.Bool {
    @objc get
    @objc set
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var isHighlighted: Swift.Bool {
    @objc get
    @objc set
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func beginTracking(_ touch: UIKit.UITouch, with event: UIKit.UIEvent?) -> Swift.Bool
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func continueTracking(_ touch: UIKit.UITouch, with event: UIKit.UIEvent?) -> Swift.Bool
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func endTracking(_ touch: UIKit.UITouch?, with event: UIKit.UIEvent?)
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func cancelTracking(with event: UIKit.UIEvent?)
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var intrinsicContentSize: CoreFoundation.CGSize {
    @objc get
  }
  @_Concurrency.MainActor(unsafe) open func animationDidSet()
  @objc deinit
}
public typealias AnimationFrameTime = CoreFoundation.CGFloat
public typealias AnimationProgressTime = CoreFoundation.CGFloat
public struct Vector1D {
  public init(_ value: Swift.Double)
  public let value: Swift.Double
}
public struct Vector3D {
  public let x: Swift.Double
  public let y: Swift.Double
  public let z: Swift.Double
  public init(x: Swift.Double, y: Swift.Double, z: Swift.Double)
}
public enum ColorFormatDenominator {
  case One
  case OneHundred
  case TwoFiftyFive
  public static func == (a: YCLottie.ColorFormatDenominator, b: YCLottie.ColorFormatDenominator) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public struct Color {
  public var r: Swift.Double
  public var g: Swift.Double
  public var b: Swift.Double
  public var a: Swift.Double
  public init(r: Swift.Double, g: Swift.Double, b: Swift.Double, a: Swift.Double, denominator: YCLottie.ColorFormatDenominator = .One)
}
public protocol AnimationCacheProvider {
  func animation(forKey: Swift.String) -> YCLottie.Animation?
  func setAnimation(_ animation: YCLottie.Animation, forKey: Swift.String)
  func clearCache()
}
public class LRUAnimationCache : YCLottie.AnimationCacheProvider {
  public init()
  public func clearCache()
  public static let sharedCache: YCLottie.LRUAnimationCache
  public var cacheSize: Swift.Int
  public func animation(forKey: Swift.String) -> YCLottie.Animation?
  public func setAnimation(_ animation: YCLottie.Animation, forKey: Swift.String)
  @objc deinit
}
public struct AnimationKeypath {
  public init(keypath: Swift.String)
  public init(keys: [Swift.String])
}
public protocol AnyValueProvider {
  var valueType: any Any.Type { get }
  func hasUpdate(frame: YCLottie.AnimationFrameTime) -> Swift.Bool
  func value(frame: YCLottie.AnimationFrameTime) -> Any
}
final public class ColorValueProvider : YCLottie.AnyValueProvider {
  public typealias ColorValueBlock = (CoreFoundation.CGFloat) -> YCLottie.Color
  final public var color: YCLottie.Color {
    get
    set
  }
  public init(block: @escaping YCLottie.ColorValueProvider.ColorValueBlock)
  public init(_ color: YCLottie.Color)
  final public var valueType: any Any.Type {
    get
  }
  final public func hasUpdate(frame: CoreFoundation.CGFloat) -> Swift.Bool
  final public func value(frame: CoreFoundation.CGFloat) -> Any
  @objc deinit
}
final public class FloatValueProvider : YCLottie.AnyValueProvider {
  public typealias CGFloatValueBlock = (CoreFoundation.CGFloat) -> CoreFoundation.CGFloat
  final public var float: CoreFoundation.CGFloat {
    get
    set
  }
  public init(block: @escaping YCLottie.FloatValueProvider.CGFloatValueBlock)
  public init(_ float: CoreFoundation.CGFloat)
  final public var valueType: any Any.Type {
    get
  }
  final public func hasUpdate(frame: CoreFoundation.CGFloat) -> Swift.Bool
  final public func value(frame: CoreFoundation.CGFloat) -> Any
  @objc deinit
}
final public class SizeValueProvider : YCLottie.AnyValueProvider {
  public typealias SizeValueBlock = (CoreFoundation.CGFloat) -> CoreFoundation.CGSize
  final public var size: CoreFoundation.CGSize {
    get
    set
  }
  public init(block: @escaping YCLottie.SizeValueProvider.SizeValueBlock)
  public init(_ size: CoreFoundation.CGSize)
  final public var valueType: any Any.Type {
    get
  }
  final public func hasUpdate(frame: CoreFoundation.CGFloat) -> Swift.Bool
  final public func value(frame: CoreFoundation.CGFloat) -> Any
  @objc deinit
}
final public class PointValueProvider : YCLottie.AnyValueProvider {
  public typealias PointValueBlock = (CoreFoundation.CGFloat) -> CoreFoundation.CGPoint
  final public var point: CoreFoundation.CGPoint {
    get
    set
  }
  public init(block: @escaping YCLottie.PointValueProvider.PointValueBlock)
  public init(_ point: CoreFoundation.CGPoint)
  final public var valueType: any Any.Type {
    get
  }
  final public func hasUpdate(frame: CoreFoundation.CGFloat) -> Swift.Bool
  final public func value(frame: CoreFoundation.CGFloat) -> Any
  @objc deinit
}
@objc final public class CompatibleAnimation : ObjectiveC.NSObject {
  @objc public init(name: Swift.String, bundle: Foundation.Bundle = Bundle.main)
  @objc deinit
}
@_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers @objc @_Concurrency.MainActor(unsafe) final public class CompatibleAnimationView : UIKit.UIView {
  @_Concurrency.MainActor(unsafe) @objc override dynamic public init(frame: CoreFoundation.CGRect)
  @objc @_Concurrency.MainActor(unsafe) final public var compatibleAnimation: YCLottie.CompatibleAnimation? {
    @objc get
    @objc set
  }
  @objc @_Concurrency.MainActor(unsafe) final public var loopAnimationCount: CoreFoundation.CGFloat {
    @objc get
    @objc set
  }
  @_Concurrency.MainActor(unsafe) @objc override final public var contentMode: UIKit.UIView.ContentMode {
    @objc get
    @objc set
  }
  @objc @_Concurrency.MainActor(unsafe) final public var shouldRasterizeWhenIdle: Swift.Bool {
    @objc get
    @objc set
  }
  @objc @_Concurrency.MainActor(unsafe) final public var currentProgress: CoreFoundation.CGFloat {
    @objc get
    @objc set
  }
  @objc @_Concurrency.MainActor(unsafe) final public var currentTime: Foundation.TimeInterval {
    @objc get
    @objc set
  }
  @objc @_Concurrency.MainActor(unsafe) final public var currentFrame: CoreFoundation.CGFloat {
    @objc get
    @objc set
  }
  @objc @_Concurrency.MainActor(unsafe) final public var realtimeAnimationFrame: CoreFoundation.CGFloat {
    @objc get
  }
  @objc @_Concurrency.MainActor(unsafe) final public var realtimeAnimationProgress: CoreFoundation.CGFloat {
    @objc get
  }
  @objc @_Concurrency.MainActor(unsafe) final public var animationSpeed: CoreFoundation.CGFloat {
    @objc get
    @objc set
  }
  @objc @_Concurrency.MainActor(unsafe) final public var respectAnimationFrameRate: Swift.Bool {
    @objc get
    @objc set
  }
  @objc @_Concurrency.MainActor(unsafe) final public var isAnimationPlaying: Swift.Bool {
    @objc get
  }
  @objc @_Concurrency.MainActor(unsafe) final public func play()
  @objc @_Concurrency.MainActor(unsafe) final public func play(completion: ((Swift.Bool) -> Swift.Void)?)
  @objc @_Concurrency.MainActor(unsafe) final public func play(fromProgress: CoreFoundation.CGFloat, toProgress: CoreFoundation.CGFloat, completion: ((Swift.Bool) -> Swift.Void)? = nil)
  @objc @_Concurrency.MainActor(unsafe) final public func play(fromFrame: CoreFoundation.CGFloat, toFrame: CoreFoundation.CGFloat, completion: ((Swift.Bool) -> Swift.Void)? = nil)
  @objc @_Concurrency.MainActor(unsafe) final public func play(fromMarker: Swift.String, toMarker: Swift.String, completion: ((Swift.Bool) -> Swift.Void)? = nil)
  @objc @_Concurrency.MainActor(unsafe) final public func stop()
  @objc @_Concurrency.MainActor(unsafe) final public func pause()
  @objc @_Concurrency.MainActor(unsafe) final public func reloadImages()
  @objc @_Concurrency.MainActor(unsafe) final public func forceDisplayUpdate()
  @objc @_Concurrency.MainActor(unsafe) final public func getValue(for keypath: YCLottie.CompatibleAnimationKeypath, atFrame: CoreFoundation.CGFloat) -> Any?
  @objc @_Concurrency.MainActor(unsafe) final public func logHierarchyKeypaths()
  @objc @_Concurrency.MainActor(unsafe) final public func setColorValue(_ color: UIKit.UIColor, forKeypath keypath: YCLottie.CompatibleAnimationKeypath)
  @objc @_Concurrency.MainActor(unsafe) final public func getColorValue(for keypath: YCLottie.CompatibleAnimationKeypath, atFrame: CoreFoundation.CGFloat) -> UIKit.UIColor?
  @objc @_Concurrency.MainActor(unsafe) final public func addSubview(_ subview: YCLottie.AnimationSubview, forLayerAt keypath: YCLottie.CompatibleAnimationKeypath)
  @objc @_Concurrency.MainActor(unsafe) final public func convert(rect: CoreFoundation.CGRect, toLayerAt keypath: YCLottie.CompatibleAnimationKeypath?) -> CoreFoundation.CGRect
  @objc @_Concurrency.MainActor(unsafe) final public func convert(point: CoreFoundation.CGPoint, toLayerAt keypath: YCLottie.CompatibleAnimationKeypath?) -> CoreFoundation.CGPoint
  @objc @_Concurrency.MainActor(unsafe) final public func progressTime(forMarker named: Swift.String) -> CoreFoundation.CGFloat
  @objc @_Concurrency.MainActor(unsafe) final public func frameTime(forMarker named: Swift.String) -> CoreFoundation.CGFloat
  @objc deinit
}
@objc final public class CompatibleAnimationKeypath : ObjectiveC.NSObject {
  @objc public init(keypath: Swift.String)
  @objc public init(keys: [Swift.String])
  final public let animationKeypath: YCLottie.AnimationKeypath
  @objc deinit
}
public enum LayerType : Swift.Int, Swift.Codable {
  case precomp
  case solid
  case image
  case null
  case shape
  case text
  public init(from decoder: any Swift.Decoder) throws
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
public enum MatteType : Swift.Int, Swift.Codable {
  case none
  case add
  case invert
  case unknown
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
public enum BlendMode : Swift.Int, Swift.Codable {
  case normal
  case multiply
  case screen
  case overlay
  case darken
  case lighten
  case colorDodge
  case colorBurn
  case hardLight
  case softLight
  case difference
  case exclusion
  case hue
  case saturation
  case color
  case luminosity
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
public enum CoordinateSpace : Swift.Int, Swift.Codable {
  case type2d
  case type3d
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
final public class Animation : Swift.Codable {
  final public let startFrame: YCLottie.AnimationFrameTime
  final public let endFrame: YCLottie.AnimationFrameTime
  final public let framerate: Swift.Double
  final public var markerNames: [Swift.String] {
    get
  }
  required public init(from decoder: any Swift.Decoder) throws
  @objc deinit
  final public func encode(to encoder: any Swift.Encoder) throws
}
final public class GradientValueProvider : YCLottie.AnyValueProvider {
  public typealias ColorsValueBlock = (CoreFoundation.CGFloat) -> [YCLottie.Color]
  public typealias ColorLocationsBlock = (CoreFoundation.CGFloat) -> [Swift.Double]
  final public var colors: [YCLottie.Color] {
    get
    set
  }
  final public var locations: [Swift.Double] {
    get
    set
  }
  public init(block: @escaping YCLottie.GradientValueProvider.ColorsValueBlock, locations: YCLottie.GradientValueProvider.ColorLocationsBlock? = nil)
  public init(_ colors: [YCLottie.Color], locations: [Swift.Double] = [])
  final public var valueType: any Any.Type {
    get
  }
  final public func hasUpdate(frame: CoreFoundation.CGFloat) -> Swift.Bool
  final public func value(frame: CoreFoundation.CGFloat) -> Any
  @objc deinit
}
public protocol AnimationTextProvider : AnyObject {
  func textFor(keypathName: Swift.String, sourceText: Swift.String) -> Swift.String
}
final public class DictionaryTextProvider : YCLottie.AnimationTextProvider {
  public init(_ values: [Swift.String : Swift.String])
  final public func textFor(keypathName: Swift.String, sourceText: Swift.String) -> Swift.String
  @objc deinit
}
final public class DefaultTextProvider : YCLottie.AnimationTextProvider {
  final public func textFor(keypathName: Swift.String, sourceText: Swift.String) -> Swift.String
  public init()
  @objc deinit
}
@_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers final public class ImageAsset : YCLottie.Asset {
  final public let name: Swift.String
  final public let directory: Swift.String
  final public let width: Swift.Double
  final public let height: Swift.Double
  override final public func encode(to encoder: any Swift.Encoder) throws
  @objc deinit
}
public class Asset : Swift.Codable {
  final public let id: Swift.String
  required public init(from decoder: any Swift.Decoder) throws
  @objc deinit
  public func encode(to encoder: any Swift.Encoder) throws
}
extension QuartzCore.CALayer {
  public func logLayerTree(withIndent: Swift.Int = 0)
}
infix operator +| : DefaultPrecedence
infix operator +- : DefaultPrecedence
extension YCLottie.Color : Swift.Codable {
  public init(from decoder: any Swift.Decoder) throws
  public func encode(to encoder: any Swift.Encoder) throws
}
public protocol AnimationFontProvider {
  func fontFor(family: Swift.String, size: CoreFoundation.CGFloat) -> CoreText.CTFont?
}
final public class DefaultFontProvider : YCLottie.AnimationFontProvider {
  final public func fontFor(family: Swift.String, size: CoreFoundation.CGFloat) -> CoreText.CTFont?
  public init()
  @objc deinit
}
extension YCLottie.Vector1D : Swift.Codable {
  public init(from decoder: any Swift.Decoder) throws
  public func encode(to encoder: any Swift.Encoder) throws
}
extension YCLottie.Vector3D : Swift.Codable {
  public init(from decoder: any Swift.Decoder) throws
  public func encode(to encoder: any Swift.Encoder) throws
}
extension YCLottie.Vector3D {
  public var pointValue: CoreFoundation.CGPoint {
    get
  }
  public var sizeValue: CoreFoundation.CGSize {
    get
  }
}
public typealias LottieCompletionBlock = (Swift.Bool) -> Swift.Void
extension YCLottie.LottieBackgroundBehavior : Swift.Equatable {}
extension YCLottie.LottieBackgroundBehavior : Swift.Hashable {}
extension YCLottie.AnimatedSwitch.CancelBehavior : Swift.Equatable {}
extension YCLottie.AnimatedSwitch.CancelBehavior : Swift.Hashable {}
extension YCLottie.ColorFormatDenominator : Swift.Equatable {}
extension YCLottie.ColorFormatDenominator : Swift.Hashable {}
extension YCLottie.LayerType : Swift.Equatable {}
extension YCLottie.LayerType : Swift.Hashable {}
extension YCLottie.LayerType : Swift.RawRepresentable {}
extension YCLottie.MatteType : Swift.Equatable {}
extension YCLottie.MatteType : Swift.Hashable {}
extension YCLottie.MatteType : Swift.RawRepresentable {}
extension YCLottie.BlendMode : Swift.Equatable {}
extension YCLottie.BlendMode : Swift.Hashable {}
extension YCLottie.BlendMode : Swift.RawRepresentable {}
extension YCLottie.CoordinateSpace : Swift.Equatable {}
extension YCLottie.CoordinateSpace : Swift.Hashable {}
extension YCLottie.CoordinateSpace : Swift.RawRepresentable {}
