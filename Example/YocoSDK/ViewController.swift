import UIKit
import YocoSDK

class ViewController: UIViewController {
  
  @IBOutlet weak var amountTextField: UITextField!
  @IBOutlet var buttonCollection: [UIButton]!
  @IBOutlet weak var scrollViewContainer: UIView!
  @IBOutlet weak var segCtrlTippingConfig: UISegmentedControl!
  @IBOutlet var printSuccessSwitch: UISwitch!
  
  private var printerConfig: PrinterConfig = PrinterConfig()
  private var transactionId: String? = nil
  private var printSuccess: Bool = false
  var tippingConfig: TippingConfig = TippingConfig.DO_NOT_ASK_FOR_TIP
  var receiptNumber:String?
  
	let merchantInfo = MerchantInfo(merchantId: "merchant-id",
									merchantName: "Boring Trends",
									phone: "0123456789",
									address: "Underground Business 1, ZA")
	
	
  func getRefundParams() -> RefundParameters {
    return RefundParameters(amountInCents: 200, userInfo: [
      "user" : "info"
    ], staffMember: YocoStaff(staffNumber: "1234",
                              name: "Joe Bloggs"), refundCompleteNotifier: { paymentResult in
      // Get notified immediately when the transaction is complete.
    })
  }
  func getPaymentParams() -> PaymentParameters {
    return PaymentParameters(receiptDelegate: nil,
                             userInfo: [
                              "user" : "info"
                             ],
                             staffMember: YocoStaff(staffNumber: "1234",
                                                    name: "Joe Bloggs"),
                             receiptNumber: self.receiptNumber,
                             billId: "test-bill-id",
                             note: "test note",
                             transactionCompleteNotifier: { paymentResult in
      // Get notified immediately when the transaction is complete.
        self.transactionId = paymentResult.transactionID
    })
  }
	
	func getPrintParams() -> PrintParameters {
		return PrintParameters(clientTransactionId: transactionId,
										merchantInfo: merchantInfo,
										transactionType: YocoTransactionType.GOODS,
										receiptType: ReceiptCopyType.BOTH,
										signature: nil,
											  metadata: ["Meta": "Data"],
		printStartListener: { result, printProgress in
			printProgress(.inProgress(message: nil))
			let seconds = 1.5
			DispatchQueue.main
				.asyncAfter(deadline: DispatchTime
					.now() + seconds)
			{
				if self.printSuccess {
					printProgress(.complete(message: nil))
				} else {
					printProgress(.error(message: "No printer"))
				}
			}
			
		}, printerStatusListener: { status in
			print("Status: \(status)")
		}, printResultListener: { result in
			print("Result: \(result)")
		})
	}
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // If something happened during a transaction, attempt to get the transaction status and display the result to the user.
    if let transactionID = UserDefaults.standard.value(forKey: "transactionInProgress") as? String {
      Yoco.getPaymentResult(transactionID: transactionID) { paymentResult, error in
        if let paymentResult = paymentResult {
          Yoco.showPaymentResult(paymentResult, paymentParameters: self.getPaymentParams()) { paymentResult in
            UserDefaults.standard.set(nil, forKey: "transactionInProgress")
          }
        }
      }
    }
	printSuccessSwitch.isOn = printSuccess
	Yoco.setPrinterConfig(printerConfig: printerConfig)
    applyTheme()
  }
  
  func applyTheme() {
    for button in buttonCollection {
      button.layer.cornerRadius = 3
    }
    
    let grey = UIColor(red: 237/255, green: 237/255, blue: 237/255, alpha: 1)
    
    scrollViewContainer.layer.cornerRadius = 8
    scrollViewContainer.layer.shadowColor = UIColor.black.cgColor
    scrollViewContainer.layer.shadowOpacity = 0.3
    scrollViewContainer.layer.shadowOffset = .zero
    scrollViewContainer.layer.shadowRadius = 5
    scrollViewContainer.backgroundColor = UIDevice.current.userInterfaceIdiom == .phone ? grey : UIColor.white
    view.backgroundColor = UIDevice.current.userInterfaceIdiom == .phone ? UIColor.white : grey
  }
  
  // MARK: IBActions
  @IBAction func autoPrintChanged(_ sender: UISwitch) {
    let printerConfig = PrinterConfig(autoPrint: sender.isOn)
    self.printerConfig = printerConfig
  }
  
  @IBAction func printingProviderChanged(_ sender: UISegmentedControl) {
	  var message = ""
	  switch sender.selectedSegmentIndex {
	  case 0:
		  let printerConfig = self.printerConfig.copy(printerProvider: PrinterProvider.none)
		  Yoco.setPrinterConfig(printerConfig: printerConfig)
		  message = "None"
	  case 1:
		  let printerConfig = self.printerConfig.copy(printerProvider: PrinterProvider.sdk)
		  Yoco.setPrinterConfig(printerConfig: printerConfig)
		  message = "SDK"
	  default:
		  let printerConfig = self.printerConfig.copy(printerProvider: PrinterProvider.external)
		  Yoco.setPrinterConfig(printerConfig: printerConfig)
		  message = "External"
	  }

	  showToast(title: "Printer Provider changed",
				message: message)
  }
  
	@IBAction func printSuccess(_ sender: UISwitch) {
		printSuccess = sender.isOn
	}
	
  @IBAction func startCardPayment(_ sender: UIButton) {
    startTransaction(type: .card)
  }
  
  @IBAction func refundPressed(_ sender: UIButton) {
    if let transactionID = UserDefaults.standard.value(forKey: "lastTransactionID") as? String {
      Yoco.refund(transactionID: transactionID, refundParameters: self.getRefundParams())
    } else {
      let alert = UIAlertController(title: "Error", message: "No transaction has been done yet to refund, please do a transaction first", preferredStyle: .alert)
      alert.addAction(UIAlertAction(title: "OK", style: .default) { action in
        alert.dismiss(animated: true)
      })
      
      present(alert, animated: true)
    }
  }
  @IBAction func lookupByReceiptNumberPressed(_ sender: UIButton) {
    if let receiptNumber = self.receiptNumber {
      Yoco.getIntegratorTransactions(receiptNumber: receiptNumber) { (result, error) in
        if let result = result {
          let alert = UIAlertController(title: "Transactions Found", message: "\(result.count) transaction(s) found", preferredStyle: .alert)
          alert.addAction(UIAlertAction(title: "OK", style: .default) { action in
            alert.dismiss(animated: true)
          })
          
          self.present(alert, animated: true)
        } else {
          let alert = UIAlertController(title: "Error", message: "Error looking up transaction \(String(describing: error?.localizedDescription))", preferredStyle: .alert)
          alert.addAction(UIAlertAction(title: "OK", style: .default) { action in
            alert.dismiss(animated: true)
          })
          
          self.present(alert, animated: true)
        }
      }
    } else {
      let alert = UIAlertController(title: "Error", message: "No transaction has been done yet to lookup, please do a transaction first", preferredStyle: .alert)
      alert.addAction(UIAlertAction(title: "OK", style: .default) { action in
        alert.dismiss(animated: true)
      })
      
      present(alert, animated: true)
    }
  }
  @IBAction func printWithLookupPressed(_ sender: UIButton) {
    if let _ = self.transactionId {
		Yoco.print(rootViewController: self,
				   printParameters: getPrintParams())
		{ _ in
		}
    } else {
      let alert = UIAlertController(title: "Error", message: "No transaction has been done yet to print, please do a transaction first", preferredStyle: .alert)
      alert.addAction(UIAlertAction(title: "OK", style: .default) { action in
        alert.dismiss(animated: true)
      })
      
      present(alert, animated: true)
    }
  }
  @IBAction func pairButtonPressed(_ sender: UIButton) {
    Yoco.pairTerminal()
  }
  
  @IBAction func logoutButtonPressed(_ sender: UIButton) {
    Yoco.logout()
    
    let alert = UIAlertController(title: "Success", message: "You have logged out successfully.", preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "OK", style: .default) { action in
      alert.dismiss(animated: true)
    })
    
    present(alert, animated: true)
  }
  
  // MARK: Transaction functions
  
  func startTransaction(type: YocoPaymentType) {
    if let amount = amountTextField.text, !amount.isEmpty {
      let cents = Double(amount)! * 100
      let money = UInt64(cents.rounded())
        self.receiptNumber = UUID().uuidString
      let transactionID = Yoco.charge(money,
                                      paymentType: type,
                                      currency: .zar,
                                      tippingConfig: tippingConfig,
                                      paymentParameters: self.getPaymentParams(),
									  printParameters: self.getPrintParams()) { paymentResult in
        // Do something once the payment is complete and the YocoSDK has dismissed
        UserDefaults.standard.set(nil, forKey: "transactionInProgress")
      }
      
      UserDefaults.standard.set(transactionID, forKey: "transactionInProgress")
      UserDefaults.standard.set(transactionID, forKey: "lastTransactionID")
    }
  }
  @IBAction func tippingConfigurationChanged(_ sender: Any) {
    switch segCtrlTippingConfig.selectedSegmentIndex {
    case 0:
      tippingConfig = TippingConfig.DO_NOT_ASK_FOR_TIP
      break
    case 1:
      tippingConfig = TippingConfig.ASK_FOR_TIP_ON_CARD_MACHINE
      break
    case 2:
      if let amount = amountTextField.text, !amount.isEmpty {
        let cents = Double(amount)! * 100
        let money = Int32(cents.rounded() * 0.1)
        
        tippingConfig = TippingConfig.INCLUDE_TIP_IN_AMOUNT(tipInCents: money)
      }
      break
    default:
      tippingConfig = TippingConfig.DO_NOT_ASK_FOR_TIP
      break
    }
  }
  
  // MARK: Receipt functions
  
  func validate(phoneNumber: String?) -> Bool {
    
    if let number = phoneNumber, number.count > 0 {
      return true
    }
    
    return false
  }
  
  func validate(emailAddress: String?) -> Bool {
    
    if let email = emailAddress, email.count > 0 {
      return true
    }
    
    return false
  }
	
	private func showToast(title: String?,
						   message: String)
	{
		let alert = UIAlertController(title: title,
									  message: message,
									  preferredStyle: .actionSheet)

		if
			let popoverController = alert
				.popoverPresentationController
		{
			popoverController.sourceView = view
			popoverController.sourceRect = CGRect(x: view.bounds.midX,
												  y: view.bounds.height,
												  width: 0,
												  height: 0)
		}

		present(alert, animated: true)
		let seconds = 1.5
		DispatchQueue.main
			.asyncAfter(deadline: DispatchTime
				.now() + seconds)
		{
			alert.dismiss(animated: true)
		}
	}
}

