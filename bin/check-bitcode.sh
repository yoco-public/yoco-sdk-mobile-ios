#!/bin/sh

#  check-bitcode.sh
#  YocoSDK_Development
#
#  Created by Omar Hussain Yoco on 21/01/2025.
#  Copyright © 2025 Yoco. All rights reserved.
cd ..
cd YocoSDK/Libraries
otool -l OnoSDK.xcframework/ios-x86_64-simulator/OnoSDK.framework/OnoSDK
otool -l OnoSDK.xcframework/ios-arm64/OnoSDK.framework/OnoSDK
otool -l YCLottie.xcframework/ios-arm64/YCLottie.framework/YCLottie
otool -l YCLottie.xcframework/ios-x86_64-simulator/YCLottie.framework/YCLottie
otool -l YCReSwift.xcframework/ios-arm64/YCReSwift.framework/YCReSwift
otool -l YCReSwift.xcframework/ios-x86_64-simulator/YCReSwift.framework/YCReSwift
otool -l YCSwiftSignatureView.xcframework/ios-arm64/YCSwiftSignatureView.framework/YCSwiftSignatureView
otool -l YCSwiftSignatureView.xcframework/ios-x86_64-simulator/YCSwiftSignatureView.framework/YCSwiftSignatureView
