// swift-tools-version: 5.10
import PackageDescription


let package = Package(
	name: "YocoSDK",
	platforms: [
		.iOS(.v12)
	],
	products: [
		.library(
			name: "YocoSDK",
			targets: ["YocoSDK", "OnoSDK", "YCLottie", "YCSwiftSignatureView", "YCReSwift", "Ono"])
	],
	dependencies: [],
	targets: [
		.binaryTarget(
			name: "YocoSDK",
			path: "./YocoSDK/YocoSDK.xcframework"
		),
		.binaryTarget(
			name: "OnoSDK",
			path: "./OnoSDK/OnoSDK.xcframework"
		),
		.binaryTarget(
			name: "YCLottie",
			path: "./YocoSDK/Libraries/YCLottie.xcframework"
		),
		.binaryTarget(
			name: "YCSwiftSignatureView",
			path: "./YocoSDK/Libraries/YCSwiftSignatureView.xcframework"
		),
		.binaryTarget(
			name: "YCReSwift",
			path: "./YocoSDK/Libraries/YCReSwift.xcframework"
		),
		.binaryTarget(
			name: "Ono",
			path: "./OnoSDK/Ono.xcframework"
		),
	]
)
